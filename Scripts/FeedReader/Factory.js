﻿app.factory('FeedService', ['$http', function ($http) {
    return {
        downloadFeed: function (_url) {
            //return $http(url);
            return $http({
                method: 'GET',
                url: 'https://api.rss2json.com/v1/api.json?rss_url=' + _url
            })
        }
    }
}])