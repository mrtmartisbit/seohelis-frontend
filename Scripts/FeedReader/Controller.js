﻿var app = angular.module('FeedApp', ['ngSanitize']);

app.controller("FeedController", ['$scope', 'FeedService', function ($scope, FeedService, $sce) {
    $scope.defaultFeedSrc = "https://techcrunch.com/feed/";
    $scope.CurrentFeed = [];//http://blog.dota2.com/?p=11266

    $scope.getFeed = function (feedUrlEntered) {
        FeedService.downloadFeed(feedUrlEntered).then(function (response) {
            $scope.CurrentFeed = response.data.items;
            console.log($scope.CurrentFeed[0]);
        });
    }

    $scope.showItemDescription = function (item) {
        $scope.SelectedItem = item;
    }

    $scope.hideItemDescription = function () {
        $scope.SelectedItem = null;
    }
}]);